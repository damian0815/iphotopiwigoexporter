//
//  IphotoLibraryDaemon.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 08/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "IphotoLibraryDaemon.h"
#import "LibraryLoader.h"
#include <dispatch/dispatch.h>
#include <errno.h>
#include <libkern/OSAtomic.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

@interface IphotoLibraryDaemonConnection: NSObject

@property (strong,readwrite,atomic) dispatch_source_t source;
@property (strong,readwrite,atomic) dispatch_source_t writeSource;
@property (assign,readwrite,atomic) int socketFd;
@property (assign,readwrite,atomic) int refCount;


@end


@implementation IphotoLibraryDaemonConnection

- (id)initWithSource:(dispatch_source_t)source onSocket:(int)socketFd
{
	self = [super init];
	if (self) {
		self.source = source;
		self.socketFd = socketFd;
		self.refCount = 2;
	}
	return self;
}

@end


@interface IphotoLibraryDaemon()

@property (strong,readwrite,atomic) LibraryLoader* loader;

@property (strong,readwrite,atomic) MLMediaGroup* eventsGroup;

@property (strong,readwrite,atomic) dispatch_source_t dispatchSource;

// array of dispatchSource for client connections
@property (strong,readwrite,atomic) NSMutableArray* connectionSources;

@end

@implementation IphotoLibraryDaemon

- (id)init
{
	self = [super init];
	if (self)
	{
		self.loader = [[LibraryLoader alloc] initWithRootGroupLoadedBlock:^(MLMediaGroup *rootGroup) {
			NSLog(@"loaded root group %@, has %i children:", rootGroup.name, (int)rootGroup.childGroups.count);
			for (MLMediaGroup* childGroup in rootGroup.childGroups) {
				NSLog(@" - %@ %@", childGroup.identifier, childGroup.name);
				if ([childGroup.identifier isEqualToString:@"AllProjectsItem"]) {
					self.eventsGroup = childGroup;
				}
			}
			
			[self.loader getPopulatedGroupAsync:self.eventsGroup loadedBlock:^(MLMediaGroup *group) {
				NSLog(@"loaded group %@, has %i children and %i objects", group.name, (int)group.childGroups.count, (int)group.mediaObjects.count);
			}];
		}];
		
		self.connectionSources = [NSMutableArray array];
		[self setupSocketsForListeningPort:12346];
	}
	return self;
}

#define CHECK(call) Check(call, __FILE__, __LINE__, #call)

static int Check(int retval, const char *file, int line, const char *name)
{
	if(retval == -1)
	{
		fprintf(stderr, "%s:%d: %s returned error %d (%s)\n", file, line, name, errno, strerror(errno));
		exit(1);
	}
	return retval;
}

- (void)setupSocketsForListeningPort:(int)port
{
	// create socket
	int listenSocket4 = CHECK(socket(PF_INET, SOCK_STREAM, 0));
	
	struct sockaddr_in addr4 = { sizeof(addr4), AF_INET, htons(port), { INADDR_ANY }, { 0 } };
	
	int yes = 1;
	CHECK(setsockopt(listenSocket4, SOL_SOCKET, SO_REUSEADDR, (void *)&yes, sizeof(yes)));
	CHECK(bind(listenSocket4, (void *)&addr4, sizeof(addr4)));
	
	self.dispatchSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, listenSocket4, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
	
	dispatch_source_set_event_handler(self.dispatchSource, ^{
		NSLog(@"got stuff!");
		
		struct sockaddr remoteAddress;
		socklen_t remoteAddressLen;
		int remoteFd = CHECK(accept(listenSocket4, &remoteAddress, &remoteAddressLen));
		NSLog(@"new connection on socket %d, new socket is %d", listenSocket4, remoteFd);

		// create connection source
		dispatch_source_t remoteSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_READ, remoteFd, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
        
		IphotoLibraryDaemonConnection* connection = [[IphotoLibraryDaemonConnection alloc] initWithSource:remoteSource  onSocket:remoteFd];
		[self.connectionSources addObject:connection];
		
		// set connection state handler
		__strong typeof(self) sSelf = self;
		dispatch_source_set_event_handler(remoteSource, ^{
			// read from the socket
			char buffer[2048];
			buffer[0] = 0;
			int bufPtr = 0;
			
            errno = 0;
            while (true) {
				int numRead = (int)read(remoteFd, buffer+bufPtr, 1);
                if (numRead==-1 || numRead==0) {
                    perror("reading from socket");
                    if (errno == EOF) {
                        NSLog(@"got EOF");
                    } else if (errno == EINTR) {
                        NSLog(@"got EINTR");
                    } else {
                        NSLog(@"err %i", errno);
                    }
                    break;
                } else if (numRead>0) {
                    //NSLog(@"read %c: %3i", buffer[bufPtr], buffer[bufPtr] );
                    /*if (buffer[bufPtr] == 0) {
                        NSLog(@"got NULL");
                        break;
                    }*/
					bufPtr += numRead;
				}
			}
            
            //buffer[bufPtr] = 0;
            
            //NSLog(@"read %s", buffer);
			
            NSData* buffData = [NSData dataWithBytes:buffer length:bufPtr];
            NSData* response = [self handleRequest:buffData];
            
			dispatch_source_t writeSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_WRITE, remoteFd, 0, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0));
			connection.writeSource = writeSource;
			dispatch_source_set_event_handler(writeSource, ^{
				//NSLog(@"writing response...");
				write(remoteFd, response.bytes, response.length);
				dispatch_source_cancel(writeSource);
			});
			dispatch_source_set_cancel_handler(writeSource, ^{
				//NSLog(@"write connection on socket %i closed", remoteFd);
				CHECK(shutdown(remoteFd, SHUT_WR));
				[sSelf releaseConnection:connection];
			});
			dispatch_resume(writeSource);
			
			dispatch_source_cancel(remoteSource);
			
		});
		dispatch_source_set_cancel_handler(remoteSource, ^{
			//NSLog(@"read connection on socket %i closed", remoteFd);
			[sSelf releaseConnection:connection];
		});
		
		dispatch_resume(remoteSource);
		
	});

	
	dispatch_resume(self.dispatchSource);
	
	CHECK(listen(listenSocket4, 16));

}


- (void)releaseConnection:(IphotoLibraryDaemonConnection*)connection
{
	@synchronized(self)
	{
		connection.refCount--;
		if (connection.refCount<=0) {
			close(connection.socketFd);
			[self.connectionSources removeObject:connection];
			NSLog(@"connection cleaned up");
		}
	}
}

- (NSData*)handleRequest:(NSData*)request
{
    NSError* err = nil;
    NSDictionary* requestObject = [NSJSONSerialization JSONObjectWithData:request options:0 error:&err];
    if (err) {
        NSLog(@"error deserializing: %@", err);
        requestObject = @{};
    }
    
    NSLog(@"decoded json to %@", requestObject);
    
    __block id responseObject = nil;
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    NSString* method = [requestObject valueForKey:@"method"];
    NSDictionary* params = [requestObject valueForKey:@"params"];
    if ([method isEqualToString:@"getRootGroup"]) {
        [self.loader getRootGroupAsyncWithLoadedBlock:^(MLMediaGroup* rootGroup) {
			if (rootGroup) {
				responseObject = [self serializeGroup:rootGroup];
			} else {
				responseObject = @{@"error": @"Root group is not available"};
			}
            dispatch_semaphore_signal(sem);
        }];
    } else if ([method isEqualToString:@"getGroup"]) {
        NSString* groupIdentifier = [params objectForKey:@"identifier"];
        [self.loader getGroupWithIdentifier:groupIdentifier loadedBlock:^(MLMediaGroup* group) {
			if (group == nil) {
				responseObject = @{@"error": [NSString stringWithFormat:@"unrecognized group: %@",groupIdentifier]};
			} else {
				responseObject = [self serializeGroup:group];
			}
            dispatch_semaphore_signal(sem);
        }];
    } else if ([method isEqualToString:@"getMediaObject"]) {
        NSString* objectIdentifier = [params objectForKey:@"objectIdentifier"];
        [self.loader getMediaObjectWithIdentifier:objectIdentifier loadedBlock:^(MLMediaObject* object) {
            responseObject = [self serializeMediaObject:object];
            dispatch_semaphore_signal(sem);
        }];
    } else {
        responseObject = @{@"error": [NSString stringWithFormat:@"unrecognized method: %@", method]};
        dispatch_semaphore_signal(sem);
    }
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    err = nil;
    NSData* responseJson = [NSJSONSerialization dataWithJSONObject:responseObject options:0 error:&err];
    if (err) {
        NSLog(@"error serializing %@: %@", responseJson, err);
        responseJson = [NSData dataWithBytes:"{}" length:2];
    }
    return responseJson;
}

- (NSDictionary*)serializeMediaObject:(MLMediaObject*)object
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    if (object.name) {
        [dict setObject:object.name forKey:@"name"];
    }
    [dict setObject:object.identifier forKey:@"identifier"];
    [dict setObject:@([object.modificationDate timeIntervalSince1970]) forKey:@"modificationDate"];
    
    NSString* mediaTypeString;
    switch (object.mediaType) {
        case MLMediaTypeAudio:
            mediaTypeString = @"audio";
            break;
        case MLMediaTypeImage:
            mediaTypeString = @"image";
            break;
        case MLMediaTypeMovie:
            mediaTypeString = @"movie";
            break;
        default:
            NSAssert(NO, @"missing case in switch");
            mediaTypeString = @"<unknown>";
            break;
    }
    [dict setObject:mediaTypeString forKey:@"mediaType"];
	if (object.contentType) {
		[dict setObject:object.contentType forKey:@"contentType"];
	}
    [dict setObject:object.name forKey:@"name"];
    [dict setObject:[object.URL absoluteString] forKey:@"URL"];
    [dict setObject:[object.originalURL absoluteString] forKey:@"originalURL"];
    [dict setObject:@(object.fileSize) forKey:@"fileSize"];
    [dict setObject:@([object.modificationDate timeIntervalSince1970]) forKey:@"modificationDate"];
    [dict setObject:[object.thumbnailURL absoluteString] forKey:@"thumbnailURL"];

    NSMutableArray* keywords = [NSMutableArray array];
    for (NSString* keyword in [object.attributes objectForKey:@"ILMediaObjectKeywordsAttribute"]) {
        [keywords addObject:keyword];
    }
    [dict setObject:keywords forKey:@"keywords"];
   
    return dict;
}

- (NSDictionary*)serializeGroup:(MLMediaGroup*)group
{
    NSMutableDictionary* dict = [NSMutableDictionary dictionary];
    
    if (group.name) {
        [dict setObject:group.name forKey:@"name"];
    }
    [dict setObject:group.identifier forKey:@"identifier"];
    [dict setObject:@([group.modificationDate timeIntervalSince1970]) forKey:@"modificationDate"];
    [dict setObject:group.mediaSourceIdentifier forKey:@"mediaSourceIdentifier"];
    [dict setObject:group.typeIdentifier forKey:@"typeIdentifier"];
    
    NSString* coverPhotoIdentifier = [group.attributes objectForKey:@"KeyPhotoKey"];
    if (coverPhotoIdentifier) {
        [dict setObject:coverPhotoIdentifier forKey:@"coverPhotoIdentifier"];
    }
    
    NSMutableDictionary* childGroups = [NSMutableDictionary dictionary];
    for (MLMediaGroup* child in group.childGroups) {
        [childGroups setObject:child.name forKey:child.identifier];
    }
    [dict setObject:childGroups forKey:@"childGroups"];
    
    NSMutableArray* childImages = [NSMutableArray array];
    for (MLMediaObject* child in group.mediaObjects) {
        [childImages addObject:child.identifier];
    }
    [dict setObject:childImages forKey:@"mediaObjects"];
    
    return dict;
}


@end
