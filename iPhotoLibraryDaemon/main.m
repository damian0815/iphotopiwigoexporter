//
//  main.m
//  iPhotoLibraryDaemon
//
//  Created by Damian Stewart on 08/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LibraryLoader.h"
#import "IphotoLibraryDaemon.h"

int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
		NSLog(@"run..");
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
			IphotoLibraryDaemon* daemon = [[IphotoLibraryDaemon alloc] init];
		});
		
		dispatch_main();
	}
    return 0;
}


