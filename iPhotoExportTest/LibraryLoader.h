//
//  LibraryLoader.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 26/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaLibrary/MediaLibrary.h>

@interface LibraryLoader : NSObject

- (id)init UNAVAILABLE_ATTRIBUTE;
- (id)initWithRootGroupLoadedBlock:(void(^)(MLMediaGroup* rootGroup))rootGroupLoadedBlock;

/// groupLoadedBlock is called immediately, either with the root group or with nil if the root group is not yet available
- (void)getRootGroupAsyncWithLoadedBlock:(void (^)(MLMediaGroup* rootGroup))groupLoadedBlock;
- (void)getPopulatedGroupAsync:(MLMediaGroup*)group loadedBlock:(void (^)(MLMediaGroup* group))groupLoadedBlock;

- (void)getGroupWithIdentifier:(NSString*)identifier loadedBlock:(void (^)(MLMediaGroup* rootGroup))groupLoadedBlock;
- (void)getMediaObjectWithIdentifier:(NSString*)mediaObject loadedBlock:(void (^)(MLMediaObject* object))objectLoadedBlock;

@end
