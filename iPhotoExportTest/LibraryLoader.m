//
//  LibraryLoader.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 26/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "LibraryLoader.h"
#import <MediaLibrary/MediaLibrary.h>

@interface LibraryLoader()

@property (strong,readwrite,atomic) MLMediaLibrary* library;
@property (strong,readwrite,atomic) MLMediaSource* iPhotoMediaSource;

/// MLMediaGroup*
@property (strong,readwrite,atomic) NSMutableArray* loadedGroups;
/// group identifier -> NSMutableArray* of groupLoadedBlock
@property (strong,readwrite,atomic) NSMutableDictionary* loadingGroups;

@property (strong,readwrite,atomic) NSString* rootGroupIdentifier;
@property (strong,readwrite,atomic) MLMediaGroup* rootGroup;


typedef void (^GroupLoadedBlock)(MLMediaGroup*);


@end

@implementation LibraryLoader

NSString* const LibraryLoaderDefaultRootGroupIdentifier = @"<<root>>";

- (id)initWithRootGroupLoadedBlock:(GroupLoadedBlock)rootGroupLoadedBlock
{
	self = [super init];
	if (self)
	{
		self.library = [[MLMediaLibrary alloc] initWithOptions:@{MLMediaLoadSourceTypesKey:@(MLMediaSourceTypeImage), MLMediaLoadIncludeSourcesKey:@[MLMediaSourceiPhotoIdentifier]}];
		
		self.loadedGroups = [NSMutableArray array];
		self.loadingGroups = [NSMutableDictionary dictionary];
		
        self.rootGroupIdentifier = LibraryLoaderDefaultRootGroupIdentifier;
        [self.loadingGroups setObject:[NSMutableArray arrayWithObject:rootGroupLoadedBlock] forKey:self.rootGroupIdentifier];
        
		NSLog(@"starting load...");
		[self.library addObserver:self forKeyPath:@"mediaSources" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionInitial context:nil];
		
	}
	return self;
}

- (MLMediaGroup*)findGroupWithIdentifier:(NSString*)identifier
{
    for (MLMediaGroup* group in self.loadedGroups) {
        if ([group.identifier isEqualToString:identifier]) {
            return group;
        }
        
        for (MLMediaGroup* child in group.childGroups) {
            if ([child.identifier isEqualToString:identifier]) {
                return child;
            }
        }
    }
    
    // not found
    return nil;
}
- (MLMediaObject*)findMediaObjectWithIdentifier:(NSString*)identifier
{
    for (MLMediaGroup* group in self.loadedGroups) {
        for (MLMediaObject* object in group.mediaObjects) {
            if ([object.identifier isEqualToString:identifier]) {
                return object;
            }
        }
    }
    
    // not found
    return nil;
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([keyPath isEqualToString:@"mediaSources"]) {
		MLMediaLibrary* library = object;
		if (library.mediaSources) {
			[self startLoadingSource];
		}
	} else if ([keyPath isEqualToString:@"rootMediaGroup"]) {
        @synchronized(self)
        {
            MLMediaGroup* rootGroup = [(MLMediaSource*)object rootMediaGroup];
            if (rootGroup) {
                self.rootGroupIdentifier = rootGroup.identifier;
                NSLog(@"loaded rootMediaGroup, getting its children...");
                NSMutableArray* initialRootGroupLoadingBlocks = [self.loadingGroups objectForKey:LibraryLoaderDefaultRootGroupIdentifier];
                [self loadGroupProperties:rootGroup loadMediaObjects:NO loadedBlock:^(MLMediaGroup *group){
                    @synchronized(self)
                    {
                        self.rootGroup = group;
                        [self.loadedGroups addObject:group];
                        for (GroupLoadedBlock block in initialRootGroupLoadingBlocks) {
                            block(group);
                        }
                    }
                }];
            }
		}
		
	} else if ([keyPath isEqualToString:@"childGroups"] || [keyPath isEqualToString:@"mediaObjects"]) {
		MLMediaGroup* group = object;
		if (group.childGroups && (group==self.iPhotoMediaSource.rootMediaGroup || group.mediaObjects)) {
			[self groupDidLoad:group];
		}
	}
}

- (void)getGroupWithIdentifier:(NSString*)identifier loadedBlock:(void (^)(MLMediaGroup* rootGroup))groupLoadedBlock
{
    @synchronized(self)
    {
        MLMediaGroup* group = [self findGroupWithIdentifier:identifier];
        if (!group) {
            groupLoadedBlock(nil);
        } else {
            [self getPopulatedGroupAsync:group loadedBlock:groupLoadedBlock];
        }
    }
}

- (void)getMediaObjectWithIdentifier:(NSString*)identifier loadedBlock:(void (^)(MLMediaObject* object))objectLoadedBlock
{
    MLMediaObject* object = [self findMediaObjectWithIdentifier:identifier];
    objectLoadedBlock(object);
}


- (void)getRootGroupAsyncWithLoadedBlock:(void (^)(MLMediaGroup* rootGroup))groupLoadedBlock
{
    @synchronized(self)
    {
		groupLoadedBlock(self.rootGroup);
    }
}

- (void)getPopulatedGroupAsync:(MLMediaGroup *)group loadedBlock:(GroupLoadedBlock)groupLoadedBlock
{
	@synchronized(self)
	{
		// find the group
		if ([self.loadedGroups containsObject:group]) {
			groupLoadedBlock(group);
		} else if ([self.loadingGroups objectForKey:group.identifier]) {
			[[self.loadingGroups objectForKey:group.identifier] addObject:groupLoadedBlock];
		} else {
			[self loadGroupProperties:group loadMediaObjects:YES loadedBlock:groupLoadedBlock];
		}
	}
}

- (void)loadGroupProperties:(MLMediaGroup*)group loadMediaObjects:(BOOL)loadMediaObjects loadedBlock:(GroupLoadedBlock)groupLoadedBlock
{
	@synchronized(self)
	{
        if ([self.loadingGroups objectForKey:group.identifier]) {
            NSLog(@"group already loading properties");
            [[self.loadingGroups objectForKey:group.identifier] addObject:groupLoadedBlock];
        } else {
            [self.loadingGroups setObject:[NSMutableArray arrayWithObject:groupLoadedBlock] forKey:group.identifier];
            // start kvo
            NSLog(@"starting kvo on group %@: %@ %@", group, group.identifier, group.name);
            [group addObserver:self forKeyPath:@"childGroups" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
            [group addObserver:self forKeyPath:@"mediaObjects" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:nil];
            [group childGroups];
            if (loadMediaObjects) {
                [group mediaObjects];
            }
        }
	}
}


- (void)startLoadingSource
{
    NSLog(@"loaded mediaSources, loading iPhotoSource...");
	MLMediaSource* iPhotoSource = [self.library.mediaSources objectForKey:@"com.apple.iPhoto"];
	if (iPhotoSource) {
		self.iPhotoMediaSource = iPhotoSource;
		[iPhotoSource addObserver:self forKeyPath:@"rootMediaGroup" options:NSKeyValueObservingOptionInitial|NSKeyValueObservingOptionNew context:NULL];
		[iPhotoSource rootMediaGroup];
    } else {
        NSLog(@"no iPhotoSource!...");
    }
}

- (void)groupDidLoad:(MLMediaGroup*)group
{
	@synchronized(self)
	{
		if (![self.loadedGroups containsObject:group]) {
            [self.loadedGroups addObject:group];
        }
	
		for (GroupLoadedBlock block in [self.loadingGroups objectForKey:group.identifier]) {
			block(group);
		}
		[self.loadingGroups removeObjectForKey:group.identifier];
	}
}

@end


