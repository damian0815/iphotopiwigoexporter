//
//  main.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 26/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreFoundation/CoreFoundation.h>
#import "LibraryLoader.h"

BOOL shouldKeepRunning = true;

int main(int argc, const char * argv[]) {
	@autoreleasepool {
	    // insert code here...
	    NSLog(@"started...");
		
		LibraryLoader* loader = [[LibraryLoader alloc] init];
		
		
		NSRunLoop* runLoop = [NSRunLoop currentRunLoop];
		while (shouldKeepRunning && [runLoop runMode:NSDefaultRunLoopMode beforeDate:[NSDate distantFuture]])
		{
		}
	}
}


