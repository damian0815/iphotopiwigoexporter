//
//  AppDelegate.m
//  iPhotoTestApp2
//
//  Created by Damian Stewart on 26/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "AppDelegate.h"
#import "MainWindowController.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

@property (strong,readwrite,atomic) MainWindowController* rootController;


@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application
    
    //self.rootController = [[MainWindowController alloc] initWithWindow:self.window];
    self.rootController = [[MainWindowController alloc] initWithWindowNibName:@"MainWindowController"];
    self.window = self.rootController.window;
    

}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}



@end
