//
//  SetGroupCoverImageOperation.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 14/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "SetGroupCoverImageOperation.h"
#import "PiwigoLibrary.h"
#import "PiwigoImage.h"
#import <AppKit/AppKit.h>
#import "NSImage+ResizeAndData.h"

@interface SetGroupCoverImageOperation()

@property (strong,readwrite,atomic) LibraryDaemonCommunicator* libraryDaemonCommunicator;
@property (strong,readwrite,atomic) PiwigoCommunicator* piwigoCommunicator;
@property (assign,readwrite,atomic) NSInteger piwigoAlbumId;
@property (copy,readwrite,atomic) NSString* groupIdentifier;

@end

@implementation SetGroupCoverImageOperation

- (id)initWithLibraryDaemonCommunicator:(LibraryDaemonCommunicator*)libraryDaemonCommunicator piwigoCommunicator:(PiwigoCommunicator*)communicator albumId:(NSInteger)piwigoAlbumId groupIdentifier:(NSString *)groupIdentifier
{
    self = [super init];
    if (self)
    {
        self.libraryDaemonCommunicator = libraryDaemonCommunicator;
        self.piwigoCommunicator = communicator;
        self.piwigoAlbumId = piwigoAlbumId;
        self.groupIdentifier = groupIdentifier;
    }
    return self;
}

- (void)main
{
    dispatch_semaphore_t sem = dispatch_semaphore_create(0);
    
    // get the group
    __block NSDictionary* groupInfo = nil;
    __block NSDictionary* keyPhotoInfo = nil;
    [self.libraryDaemonCommunicator getGroupWithIdentifier:self.groupIdentifier completionBlock:^(NSDictionary *groupData) {
        groupInfo = groupData;
        NSString* keyPhotoKey = [groupData objectForKey:@"coverPhotoIdentifier"];
        [self.libraryDaemonCommunicator getMediaObjectWithIdentifier:keyPhotoKey completionBlock:^(NSDictionary *objectData) {
            keyPhotoInfo = objectData;
            dispatch_semaphore_signal(sem);
        }];
    }];
    
    dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
    
    
    // get the album
    PiwigoAlbum* album = [[PiwigoLibrary sharedLibrary] getAlbumWithId:self.piwigoAlbumId];
    
    if (album.images.count>0) {
        NSString* keyPhotoKey = [keyPhotoInfo objectForKey:@"identifier"];
        NSString* keyPhotoKeySquare = [keyPhotoKey stringByAppendingString:@"_square"];
        __block PiwigoImage* coverImage = [album imageWithId:album.coverImageId];
        if ([coverImage.iPhotoKey isEqualToString:keyPhotoKey] || [coverImage.iPhotoKey isEqualToString:keyPhotoKeySquare]) {
            // nothing to do
        } else {
            // if the image is square, we can just assign it.
            // otherwise, we need to crop it and upload a smaller version.
            
            // load the image
            NSString* path = [[NSURL URLWithString:[keyPhotoInfo objectForKey:@"URL"]] path];
            NSImage* image = [[NSImage alloc] initWithContentsOfFile:path];
            BOOL needToCrop = NO;
            if (!image) {
                NSLog(@"unable to load image from %@", path);
            } else {
                NSSize imageSize = image.size;
                if ((int)imageSize.width != (int)imageSize.height) {
                    // need to crop
                    needToCrop = YES;
                }
                
                if (needToCrop) {
                    
                    if ([album imageWithIphotoKey:keyPhotoKeySquare]) {
                        // already have a square image, so get it
						coverImage = [album imageWithIphotoKey:keyPhotoKeySquare];
                    } else {
						// need to add a squarified cover image
                        CGFloat cropSize = MIN(imageSize.width,imageSize.height);
						CGFloat outputSize = MIN(cropSize,2048);
                        
                        // for landscape, use mid-x. for portrait, use top-y.
                        CGFloat x = 0;
                        CGFloat y = 0;
                        if (imageSize.width>imageSize.height) {
                            x = imageSize.width/2 - cropSize/2;
                        } else {
                            y = imageSize.height-cropSize;
                        }
                        NSRect sourceRect = NSMakeRect(x, y, cropSize, cropSize);
                        NSImage* newImage = [image croppedImageWithSize:NSMakeSize(outputSize, outputSize) cropRect:sourceRect];
                        
                        NSBitmapImageRep* imageRep = [newImage unscaledBitmapImageRep];
                        NSData* data = [imageRep representationUsingType:NSJPEGFileType properties:@{NSImageCompressionFactor:@(0.5f)}];
                        
                        NSLog(@"uploading squarified cover image...");
                        NSString* comment = [PiwigoImage commentForIphotoKey:keyPhotoKeySquare];
                        NSArray* tagIds = @[];
                        [self.piwigoCommunicator addImageWithData:data toAlbum:self.piwigoAlbumId comment:comment tagIds:tagIds successBlock:^(NSInteger newImageId) {
                            coverImage = [[PiwigoImage alloc] initWithId:newImageId comment:comment];
                            coverImage.tagIds = tagIds;
                            [album addImage:coverImage];
                            dispatch_semaphore_signal(sem);
                        } failBlock:^(NSArray *errors) {
                            dispatch_semaphore_signal(sem);
                        }];
                        
                        dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
                    }
                }
            }
			
            // update piwigo cover image index
            [self.piwigoCommunicator setAlbumCoverImageForAlbum:self.piwigoAlbumId toImage:coverImage.imageId successBlock:^(NSXMLDocument *imagesElement) {
				dispatch_semaphore_signal(sem);
			} failBlock:^(NSError *error) {
				NSLog(@"error setting cover image: %@", error);
				dispatch_semaphore_signal(sem);
			}];
			
			dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        }
    }
}

@end
