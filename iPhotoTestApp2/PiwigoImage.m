//
//  PiwigoImage.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 03/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "PiwigoImage.h"

@interface PiwigoImage()

@property (assign,readwrite,atomic) NSInteger imageId;
@property (strong,readwrite,atomic) NSString* comment;
@property (assign,readwrite,atomic) NSSize size;

@end

@implementation PiwigoImage

+ (NSString*)commentForIphotoKey:(NSString*)iPhotoKey
{
    return [NSString stringWithFormat:@"{iPhotoKey:%@}", iPhotoKey];
}

- (id)initWithId:(NSInteger)imageId comment:(NSString *)comment
{
    self = [super init];
    if (self) {
        self.imageId = imageId;
        self.comment = comment;
        self.tagIds = [NSArray array];
    }
    return self;
}

- (id)initWithXml:(NSXMLElement*)imageXml
{
    NSInteger imageId = [[[imageXml attributeForName:@"id"] stringValue] integerValue];
    NSString* comment = [[[imageXml elementsForName:@"comment"] firstObject] stringValue];
    return [self initWithId:imageId comment:comment];
}

- (NSString*)iPhotoKey
{
    if (self.comment == nil) {
        return nil;
    } else {
        NSError* err = nil;
        NSRegularExpression* regex = [NSRegularExpression regularExpressionWithPattern:@"^.*\\{iPhotoKey:(.*)\\}.*$" options:0 error:&err];
        NSRange range = NSMakeRange(0, self.comment.length);
        NSString* iPhotoKey = [regex stringByReplacingMatchesInString:self.comment options:0 range:range withTemplate:@"$1"];
        return iPhotoKey;
    }
    
}

@end
