//
//  LibraryDaemonCommunicator.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 10/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LibraryDaemonCommunicator : NSObject

- (id)initWithPort:(int)port;

- (void)getRootGroupWithCompletionBlock:(void (^)(NSDictionary* rootGroupData)) completionBlock;
- (void)getGroupWithIdentifier:(NSString*)identifier completionBlock:(void (^)(NSDictionary* groupData))completionBlock;
- (void)getMediaObjectWithIdentifier:(NSString*)identifier completionBlock:(void (^)(NSDictionary* objectData))completionBlock;

                                         
- (void)makeRequestWithMethod:(NSString*)method params:(NSDictionary*)params completionBlock:(void (^)(id object))completionBlock;

@end
