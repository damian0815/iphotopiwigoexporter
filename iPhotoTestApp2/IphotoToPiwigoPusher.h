//
//  IphotoToPiwigoPusher.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 01/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IphotoToPiwigoPusher : NSObject

@property (strong,readwrite,atomic) void (^progressBlock)(CGFloat progress);
@property (strong,readwrite,atomic) void (^completionBlock)(BOOL wasCancelled);

- (id)init UNAVAILABLE_ATTRIBUTE;
- (id)initWithPiwigoUrl:(NSString*)url username:(NSString *)username password:(NSString *)password;

- (void)start;

- (void)cancel;

@end
