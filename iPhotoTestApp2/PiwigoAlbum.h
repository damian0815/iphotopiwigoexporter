//
//  PiwigoAlbum.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 02/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>

@class PiwigoImage;

@interface PiwigoAlbum : NSObject

@property (readonly,atomic) NSInteger albumId;
@property (readonly,atomic) NSInteger parentAlbumId; // -1 if no parent
@property (readonly,atomic) NSInteger coverImageId;
@property (readonly,atomic) NSString* name;

@property (readonly,atomic) NSArray* images;

- (id)init UNAVAILABLE_ATTRIBUTE;
- (id)initWithAlbumXml:(NSXMLElement*)element;
- (id)initWithName:(NSString*)name id:(NSInteger)albumId parentId:(NSInteger)parentAlbumId coverImageId:(NSInteger)coverImageId;

- (void)setImagesXml:(NSArray*)elements;
- (void)addImage:(PiwigoImage*)image;

- (PiwigoImage*)imageWithIphotoKey:(NSString*)key;
- (PiwigoImage*)imageWithId:(NSInteger)imageId;

@end
