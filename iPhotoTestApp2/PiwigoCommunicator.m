//
//  PiwigoCommunicator.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 28/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "PiwigoCommunicator.h"
#import "AFNetworking.h"
#import "NSData+MD5Digest.h"
#import "NSData+Base64.h"
#import <AppKit/AppKit.h>
#import "NSImage+ResizeAndData.h"

NSString* const PiwigoCommunicatorErrorDomain = @"PiwigoCommunicator";

@interface PiwigoCommunicator()

@property (strong,readwrite,atomic) AFHTTPRequestOperationManager* requestManager;

@property (strong,readwrite,atomic) NSURL* url;
@property (strong,readwrite,atomic) NSString* username;
@property (strong,readwrite,atomic) NSString* password;

@end

@implementation PiwigoCommunicator

- (id)initWithPiwigoUrl:(NSURL*)url username:(NSString*)username password:(NSString*)password
{
	self = [super init];
	if (self)
	{
		self.url = url;
		self.username = username;
		self.password = password;
		
		self.requestManager = [AFHTTPRequestOperationManager manager];
		self.requestManager.responseSerializer = [[AFXMLDocumentResponseSerializer alloc] init];
	}
	return self;
}

- (void)loginWithSuccessBlock:(void (^)())successBlock failBlock:(void (^)(NSError* error))failBlock
{
	[self issueRequestToPiwigoMethod:@"pwg.session.login" data:@{@"username":self.username, @"password":self.password} completionBlock:^(AFHTTPRequestOperation *operation, id object) {
		successBlock();
	} failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
		if (failBlock) {
			failBlock(error);
		}
	}];
}


- (void)getAlbumsWithSuccessBlock:(void (^)(NSXMLDocument*))successBlock failBlock:(void (^)(NSError *))failBlock
{
	[self issueRequestToPiwigoMethod:@"pwg.categories.getList" data:@{@"recursive":@"true"} completionBlock:^(AFHTTPRequestOperation *operation, id object) {
		successBlock(object);
	} failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
		failBlock(error);
	}];
}
- (void)getTagsWithSuccessBlock:(void (^)(NSXMLDocument*))successBlock failBlock:(void (^)(NSError *))failBlock
{
	[self issueRequestToPiwigoMethod:@"pwg.tags.getAdminList" data:@{} completionBlock:^(AFHTTPRequestOperation *operation, id object) {
		successBlock(object);
	} failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
		failBlock(error);
	}];
}

- (void)addAlbumWithName:(NSString*)name parentId:(NSInteger)parentId public:(BOOL)isPublic successBlock:(void(^)(NSXMLDocument*))successBlock failBlock:(void(^)(NSError*))failBlock
{
    NSMutableDictionary* data = [NSMutableDictionary dictionary];
    [data setObject:name forKey:@"name"];
    if (parentId != -1) {
        [data setObject:@(parentId) forKey:@"parent"];
    }
    [data setObject:isPublic?@"public":@"private" forKey:@"status"];
    [self issueRequestToPiwigoMethod:@"pwg.categories.add" data:data completionBlock:^(AFHTTPRequestOperation *operation, id object) {
        successBlock(object);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        failBlock(error);
    }];
}


- (void)getImagesInAlbum:(NSInteger)albumId successBlock:(void (^)(NSXMLDocument* imagesDocument))successBlock failBlock:(void (^)(NSError* error))failBlock
{
    [self issueRequestToPiwigoMethod:@"pwg.categories.getImages" data:@{@"cat_id":@(albumId), @"per_page":@(100000)} completionBlock:^(AFHTTPRequestOperation *operation, id object) {
        successBlock(object);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        failBlock(error);
    }];
}

- (void)setAlbumCoverImageForAlbum:(NSInteger)albumId toImage:(NSInteger)imageId successBlock:(void (^)(NSXMLDocument* imagesElement))successBlock failBlock:(void (^)(NSError* error))failBlock
{
    [self issueRequestToPiwigoMethod:@"pwg.categories.setRepresentative" data:@{@"category_id":@(albumId), @"image_id":@(imageId)} completionBlock:^(AFHTTPRequestOperation *operation, id object) {
        if (successBlock) {
            successBlock(object);
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        if (failBlock) {
            failBlock(error);
        } else {
            NSLog(@"error setting cover image %i for album %i: %@", (int)albumId, (int)imageId, error);
        }
    }];
}

- (void)addTagWithName:(NSString*)name successBlock:(void(^)(NSInteger tagId))successBlock failBlock:(void (^)(NSError* err))failBlock
{
    [self issueRequestToPiwigoMethod:@"pwg.tags.add" data:@{@"name":name} autoEnqueue:YES requestMethod:@"GET"  completionBlock:^(AFHTTPRequestOperation *operation, id object) {
        NSError* err = [self extractAnErrorFromPiwigoResponse:object];
        if (err) {
            failBlock(err);
        } else {
            NSError* err = nil;
            NSString* tagIdString = [[[object nodesForXPath:@"//rsp/id" error:&err] firstObject] stringValue];
            if (err || !tagIdString) {
                failBlock([NSError errorWithDomain:PiwigoCommunicatorErrorDomain code:102 userInfo:@{NSLocalizedDescriptionKey:@"Unable to get id from response", @"response":object}]);
            } else {
                successBlock([tagIdString integerValue]);
            }
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        failBlock(error);
    }];
}

- (void)setTagsOnImageWithId:(NSInteger)imageId toTagIds:(NSArray*)tagIds successBlock:(void (^)(NSXMLDocument*))successBlock failBlock:(void (^)(NSError* error))failBlock
{
    [self issueRequestToPiwigoMethod:@"pwg.images.setInfo" data:@{@"image_id":@(imageId), @"tag_ids":[tagIds componentsJoinedByString:@","]} completionBlock:^(AFHTTPRequestOperation *operation, id object) {
        successBlock(object);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        failBlock(error);
    }];
}

- (void)addImageWithData:(NSData*)data toAlbum:(NSInteger)albumId comment:(NSString *)comment tagIds:(NSArray*)tagIds successBlock:(void (^)(NSInteger newImageId))successBlock failBlock:(void (^)(NSArray * errors))failBlock
{
    NSString* md5 = [data MD5HexDigest];
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSMutableArray* errors = [NSMutableArray array];
    
	[self uploadImageChunk:data type:@"file" md5:md5 dispatchGroup:dispatchGroup onError:^(NSError* err) {
		@synchronized(errors) {
			[errors addObject:err];
		}
	}];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        // wait for completion on a background thread
        dispatch_group_wait(dispatchGroup, DISPATCH_TIME_FOREVER);
        
        if (errors.count) {
            failBlock(errors);
        } else {
            NSDictionary* dict = @{@"original_sum": md5, @"categories": @(albumId), @"comment": comment, @"tag_ids": [tagIds componentsJoinedByString:@","] };
            
            [self addImageWithImageData:dict successBlock:successBlock failBlock:failBlock];
        }
    });
    
}

- (void)addImageAtURL:(NSURL *)fileUrl thumbnail:(NSURL*)thumbnailUrl toAlbum:(NSInteger)albumId comment:(NSString *)comment tagIds:(NSArray*)tagIds successBlock:(void (^)(NSInteger imageId))successBlock failBlock:(void (^)(NSArray * errors))failBlock
{
    NSData* fileData = [NSData dataWithContentsOfURL:fileUrl];
	if (!fileData) {
		failBlock([NSError errorWithDomain:PiwigoCommunicatorErrorDomain code:103 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Unable to load contents of file %@", [fileUrl path]]}]);
		return;
	}
    NSString* md5 = [fileData MD5HexDigest];
    dispatch_group_t dispatchGroup = dispatch_group_create();
    NSMutableArray* errors = [NSMutableArray array];
	
	NSImage* original = [[NSImage alloc] initWithData:fileData];
	if (!original) {
		failBlock([NSError errorWithDomain:PiwigoCommunicatorErrorDomain code:104 userInfo:@{NSLocalizedDescriptionKey:[NSString stringWithFormat:@"Couldn't load image from %@",fileUrl]}]);
		return;
	}
	
	NSString* fileChunkName = @"file";
	NSSize originalSize = original.size;
	if (originalSize.width>2048 || originalSize.height>2048) {
        // downsize
		NSSize newSize;
		CGFloat scale = 2048.0f/MAX(originalSize.width,originalSize.height);
		newSize.width = originalSize.width*scale;
		newSize.height = originalSize.height*scale;
        NSImage* resized = [original resizedImageWithSize:newSize];
        
        // convert to NSData
		NSBitmapImageRep* imageRep = [resized unscaledBitmapImageRep];
		NSData* resizedData = [imageRep representationUsingType:NSJPEGFileType properties:@{NSImageCompressionFactor:@(0.5f)}];
        
        // upload
		[self uploadImageChunk:resizedData type:@"file" md5:md5 dispatchGroup:dispatchGroup onError:^(NSError *err) {
			@synchronized(errors) {
				[errors addObject:err];
			}
		}];
		fileChunkName = @"original";
	}
	
	[self uploadImageChunk:fileData type:fileChunkName md5:md5 dispatchGroup:dispatchGroup onError:^(NSError* err) {
		@synchronized(errors) {
			[errors addObject:err];
		}
	}];
	
	NSData* thumbData = [NSData dataWithContentsOfURL:thumbnailUrl];
	[self uploadImageChunk:thumbData type:@"thumb" md5:md5 dispatchGroup:dispatchGroup onError:^(NSError *err) {
		@synchronized(errors) {
			[errors addObject:err];
		}
	}];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^() {
        // wait for completion on a background thread
        dispatch_group_wait(dispatchGroup, DISPATCH_TIME_FOREVER);
        
        if (errors.count) {
            failBlock(errors);
        } else {
            NSDictionary* dict = @{@"original_sum": md5, @"categories": @(albumId), @"original_filename":[fileUrl lastPathComponent], @"comment": comment, @"tag_ids": [tagIds componentsJoinedByString:@","] };
            
            [self addImageWithImageData:dict successBlock:successBlock failBlock:failBlock];
        }
    });
}

- (void)addImageWithImageData:(NSDictionary*)dict successBlock:(void (^)(NSInteger imageId))successBlock failBlock:(void (^)(NSArray * errors))failBlock
{
    // add the actual image
    [self issueRequestToPiwigoMethod:@"pwg.images.add" data:dict completionBlock:^(AFHTTPRequestOperation *operation, NSXMLDocument* object) {
        
        NSError* err = [self extractAnErrorFromPiwigoResponse:object];
        if (err) {
            failBlock(@[err]);
        } else {
            NSError* err = nil;
            NSInteger imageId = [[[[object nodesForXPath:@"//rsp/image_id" error:&err] firstObject] stringValue] integerValue];
            if (err) {
                failBlock([NSError errorWithDomain:PiwigoCommunicatorErrorDomain code:102 userInfo:@{NSLocalizedDescriptionKey:@"Unable to get image_id from response", @"response":object}]);
            } else {
                successBlock(imageId);
            }
        }
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        failBlock(@[error]);
    }];
    
}



- (void)getImageInfoForImageWithId:(NSInteger)imageId successBlock:(void (^)(NSXMLDocument* imageInfo))successBlock failBlock:(void (^)(NSError* error))failBlock
{
    [self issueRequestToPiwigoMethod:@"pwg.images.getInfo" data:@{@"image_id": @(imageId)} autoEnqueue:YES requestMethod:@"POST" completionBlock:^(AFHTTPRequestOperation *operation, id object) {
        successBlock(object);
    } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
        failBlock(error);
    }];
    
}

- (NSError*)extractAnErrorFromPiwigoResponse:(NSXMLDocument*)object
{
    
    NSXMLElement* rspElement = [[[object rootElement] elementsForName:@"rsp"] firstObject];
    if ([[[rspElement attributeForName:@"stat"] stringValue] isEqualToString:@"fail"]) {
        if ([rspElement elementsForName:@"err"].count > 0) {
            NSXMLElement* errElement = [[rspElement elementsForName:@"err"] firstObject];
            
            NSError* err = [NSError errorWithDomain:PiwigoCommunicatorErrorDomain code:101 userInfo:@{@"code":[errElement attributeForName:@"code"], @"msg": [errElement attributeForName:@"msg"], @"completeResponse":object}];
            return err;
        }
    }
    // no error
    return nil;
}

- (void)uploadImageChunk:(NSData*)data type:(NSString*)type md5:(NSString*)md5 dispatchGroup:(dispatch_group_t)dispatchGroup onError:(void(^)(NSError* err))onErrorBlock
{
    NSUInteger position = 0;
    static const NSUInteger CHUNK_SIZE=512*1024;

    int count = 0;
    while (position < data.length) {
        NSMutableDictionary* dict = [[NSMutableDictionary alloc] init];
        [dict setObject:md5 forKey:@"original_sum"];
        [dict setObject:type forKey:@"type"];
        [dict setObject:@(count) forKey:@"position"];
        count++;
        
        NSUInteger chunkLength = MIN(CHUNK_SIZE, data.length-position);
        NSRange chunkRange = NSMakeRange(position, chunkLength);
        NSData* chunkData = [data subdataWithRange:chunkRange];
        [dict setObject:[chunkData base64EncodedString] forKey:@"data"];
        //NSLog(@"queueing bytes %lu-%lu for %@", chunkRange.location, chunkRange.location+chunkRange.length, type);
        
        dispatch_group_enter(dispatchGroup);
        AFHTTPRequestOperation* thisOperation = [self issueRequestToPiwigoMethod:@"pwg.images.addChunk" data:dict autoEnqueue:YES requestMethod:@"POST" completionBlock:^(AFHTTPRequestOperation *operation, id object) {
            //NSLog(@"uploaded bytes %lu-%lu for %@, response %@", chunkRange.location, chunkRange.location+chunkRange.length, type, object);
            dispatch_group_leave(dispatchGroup);
            
        } failureBlock:^(AFHTTPRequestOperation *operation, NSError *error) {
            onErrorBlock(error);
            dispatch_group_leave(dispatchGroup);
        }];
        
        position += chunkLength;
    }
}

- (AFHTTPRequestOperation*)issueRequestToPiwigoMethod:(NSString*)method data:(NSDictionary*)data completionBlock:(void (^)(AFHTTPRequestOperation* operation, id object))completionBlock failureBlock:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failureBlock
{
    return [self issueRequestToPiwigoMethod:method data:data autoEnqueue:YES requestMethod:@"POST" completionBlock:completionBlock failureBlock:failureBlock];
}

- (AFHTTPRequestOperation*)issueRequestToPiwigoMethod:(NSString*)method data:(NSDictionary*)data autoEnqueue:(BOOL)autoEnqueue requestMethod:(NSString*)requestMethod completionBlock:(void (^)(AFHTTPRequestOperation* operation, id object))completionBlock failureBlock:(void (^)(AFHTTPRequestOperation* operation, NSError* error))failureBlock;
{
	NSString* url = [[self.url URLByAppendingPathComponent:@"ws.php"] absoluteString];
    
	NSMutableDictionary* params = [[NSMutableDictionary alloc] init];
    [params setObject:@"rest" forKey:@"format"];
	[params setObject:method forKey:@"method"];
	
	[data enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
		[params setObject:obj forKey:key];
	}];
    
    NSError* err = nil;
    NSURLRequest* request = [[self.requestManager requestSerializer] requestWithMethod:requestMethod URLString:url parameters:params error:&err];
	
    AFHTTPRequestOperation* operation = [self.requestManager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, NSXMLDocument* responseObject) {
       	if (completionBlock) {
			completionBlock(operation, responseObject);
		}
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
       	if (failureBlock) {
			failureBlock(operation, error);
		} else {
			NSLog(@"%@ failed with error %@", method, error);
		}
    }];
    
    if (autoEnqueue) {
        [self.requestManager.operationQueue addOperation:operation];
    }
    
    return operation;
}

- (void)waitForCompletionWithBlock:(void (^)())completionBlock
{
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^() {
		[self.requestManager.operationQueue waitUntilAllOperationsAreFinished];
		
		dispatch_async(dispatch_get_main_queue(), ^() {
			completionBlock();
		});
	});
	
}



@end