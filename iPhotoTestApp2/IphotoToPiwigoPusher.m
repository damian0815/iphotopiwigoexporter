//
//  IphotoToPiwigoPusher.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 01/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <MediaLibrary/MediaLibrary.h>
#import "IphotoToPiwigoPusher.h"
#import "PiwigoCommunicator.h"
#import "PiwigoLibrary.h"
#import "PiwigoImage.h"
#import "LibraryDaemonCommunicator.h"

#import "ProcessGroupOperation.h"
#import "ProcessImageOperation.h"
#import "SetGroupCoverImageOperation.h"

@interface IphotoToPiwigoPusher()

@property (strong,readwrite,atomic) PiwigoCommunicator* piwigoCommunicator;
@property (strong,readwrite,atomic) LibraryDaemonCommunicator* libraryDaemonCommunicator;

@property (strong,readwrite,atomic) PiwigoLibrary* piwigoLibrary;

@property (strong,readwrite,atomic) NSMutableDictionary* groupImageTotalCounts;
@property (strong,readwrite,atomic) NSMutableDictionary* groupImageProcessedCounts;

@property (assign,readwrite,atomic) BOOL cancellationPending;

@property (strong,readwrite,atomic) NSOperationQueue* processQueue;

@end


@implementation IphotoToPiwigoPusher

- (id)initWithPiwigoUrl:(NSString*)url username:(NSString *)username password:(NSString *)password
{
	self = [super init];
	if (self)
	{
		self.piwigoCommunicator = [[PiwigoCommunicator alloc] initWithPiwigoUrl:[NSURL URLWithString:url] username:username password:password];
		
		self.processQueue = [[NSOperationQueue alloc] init];
		self.processQueue.maxConcurrentOperationCount = 2;
		
		self.groupImageProcessedCounts = [NSMutableDictionary dictionary];
		self.groupImageTotalCounts = [NSMutableDictionary dictionary];
	}
	
	return self;
}

- (void)start
{
    NSAssert(!self.libraryDaemonCommunicator, @"already started!");
	self.libraryDaemonCommunicator = [[LibraryDaemonCommunicator alloc] initWithPort:12346];
    
    __strong typeof(self) sSelf = self;
    void (^goBlock)() = ^{
        [self.libraryDaemonCommunicator getRootGroupWithCompletionBlock:^(NSDictionary* rootGroupData){
			if (!rootGroupData) {
				NSLog(@"unable to connect");
				[self finish];
			} else if ([rootGroupData objectForKey:@"error"]) {
				NSLog(@"error getting root group: %@", [rootGroupData objectForKey:@"error"]);
				[self finish];
			} else {
				NSLog(@"got root group: %@", rootGroupData);
				
				// process events group
				NSOperation* finalOperation = [NSBlockOperation blockOperationWithBlock:^(){
					NSLog(@"finished processing Events");
					[sSelf finish];
				}];
				[self addProcessGroupOperationForGroupWithName:@"Events" identifier:@"AllProjectsItem" finalOperation:finalOperation];
				[[NSOperationQueue mainQueue] addOperation:finalOperation];
				 
			}
        }];
    };
    
	[self.piwigoCommunicator loginWithSuccessBlock:^{
		NSLog(@"logged in");
        if ([PiwigoLibrary sharedLibraryInitialized]) {
            goBlock();
        } else {
            [self.piwigoCommunicator getAlbumsWithSuccessBlock:^(id albums) {
                NSLog(@"got albums: %@", albums);
                
                [self.piwigoCommunicator getTagsWithSuccessBlock:^(NSXMLDocument* tags) {
                    NSLog(@"got tags: %@", tags);
                    // init piwigo library
                    self.piwigoLibrary = [[PiwigoLibrary alloc] initWithAlbumsXml:albums tagsXml:tags];
                    
                    // ok, go
                    goBlock();
                    
				} failBlock:^(NSError *err) {
					NSLog(@"error getting tags: %@", err);
					[self finish];
				}];
			} failBlock:^(NSError *err) {
				NSLog(@"error getting albums: %@", err);
				[self finish];
			}];
        }
    } failBlock:^(NSError *error) {
        NSLog(@"unable to login: %@", error);
        [self finish];
    }];
    
    
}

- (NSOperation*)addProcessGroupOperationForGroupWithName:(NSString*)name identifier:(NSString*)groupId finalOperation:(NSOperation*)finalOperation
{
	__strong typeof(self) sSelf = self;
	ProcessGroupOperation* operation = [[ProcessGroupOperation alloc] initWithLibraryDaemonCommunicator:self.libraryDaemonCommunicator piwigoCommunicator:self.piwigoCommunicator groupName:name groupIdentifier:groupId];
	[operation setQueuePriority:NSOperationQueuePriorityHigh];
	
	__weak typeof(operation) wOperation = operation;
	operation.completionBlock = ^() {
		__strong typeof(operation) sOperation = wOperation;
		
        // enqueue child group operations
		[sOperation.childGroups enumerateKeysAndObjectsUsingBlock:^(NSString* identifier, NSString* name, BOOL *stop){
			[sSelf addProcessGroupOperationForGroupWithName:name identifier:identifier finalOperation:finalOperation];
		}];
        
        // enque child media object operations
        if (sOperation.childMediaObjectIds.count) {
            SetGroupCoverImageOperation* setCoverImageOp = [[SetGroupCoverImageOperation alloc] initWithLibraryDaemonCommunicator:self.libraryDaemonCommunicator piwigoCommunicator:self.piwigoCommunicator albumId:sOperation.piwigoAlbumId groupIdentifier:groupId];
			setCoverImageOp.queuePriority = NSOperationQueuePriorityLow;
			
			@synchronized(self) {
				self.groupImageTotalCounts[@(sOperation.piwigoAlbumId)] = @(sOperation.childMediaObjectIds.count);
				self.groupImageProcessedCounts[@(sOperation.piwigoAlbumId)] = @(0);
				[self updateProgress];
			}
			
            for (NSString* objectIdentifier in sOperation.childMediaObjectIds) {
                NSOperation* op = [sSelf addProcessImageOperationForImageWithId:objectIdentifier inAlbumWithId:sOperation.piwigoAlbumId];
                [setCoverImageOp addDependency:op];
            }
			
            [self.processQueue addOperation:setCoverImageOp];
			[finalOperation addDependency:setCoverImageOp];
        }
	};
	[finalOperation addDependency:operation];
	
	[self.processQueue addOperation:operation];
    return operation;
}

- (NSOperation*)addProcessImageOperationForImageWithId:(NSString*)imageId inAlbumWithId:(NSInteger)piwigoAlbumId
{
	ProcessImageOperation* operation = [[ProcessImageOperation alloc] initWithLibraryDaemonCommunicator:self.libraryDaemonCommunicator piwigoCommunicator:self.piwigoCommunicator imageIdentifier:imageId piwigoAlbumId:piwigoAlbumId];
	__weak typeof(operation) wOperation = operation;
	operation.completionBlock = ^() {
		__strong typeof(operation) sOperation = wOperation;
        if (sOperation.isFinished) {
            NSLog(@"processed image %@ in album %li, errors %@", imageId, (long)piwigoAlbumId, sOperation.errors);
			@synchronized(self) {
				self.groupImageProcessedCounts[@(piwigoAlbumId)] = @([self.groupImageProcessedCounts[@(piwigoAlbumId)] intValue]+1);
				[self updateProgress];
			}
        }
	};
	
	[self.processQueue addOperation:operation];
    return operation;
}

- (void)cancel
{
	self.cancellationPending = true;
    [self.processQueue cancelAllOperations];
    
}

- (void)finish
{
	// wait for afnetworking to finish
	NSLog(@"waiting for piwigo communicator to finish...");
	[self.piwigoCommunicator waitForCompletionWithBlock:^() {
		NSLog(@"piwigo communicator finished");
		if (self.completionBlock) {
			self.completionBlock(self.cancellationPending);
		}
	}];
}




- (void)updateProgress
{
	dispatch_async(dispatch_get_main_queue(), ^() {
		@synchronized(self)
		{
			if (self.progressBlock) {
				NSUInteger total = 0;
				for (NSNumber* count in self.groupImageTotalCounts.allValues) {
					total += [count intValue];
				}
				NSUInteger processed = 0;
				for (NSNumber* count in self.groupImageProcessedCounts.allValues) {
					processed += [count intValue];
				}
				
				if (total>0) {
					CGFloat progress = (CGFloat)processed / (CGFloat)total;
					self.progressBlock(progress);
				}
			}
		}
	});
}


@end
