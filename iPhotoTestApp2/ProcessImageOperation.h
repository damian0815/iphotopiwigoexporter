//
//  ProcessImageOperation.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 12/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "LibraryDaemonCommunicator.h"
#import "PiwigoCommunicator.h"

@interface ProcessImageOperation : NSOperation

@property (readonly,copy,atomic) NSArray* errors;

- (id)initWithLibraryDaemonCommunicator:(LibraryDaemonCommunicator*)libraryDaemonCommunicator piwigoCommunicator:(PiwigoCommunicator*)piwigoCommunicator imageIdentifier:(NSString*)imageId piwigoAlbumId:(NSInteger)piwigoAlbumId;

@end
