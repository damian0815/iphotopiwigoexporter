//
//  ProcessImageOperation.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 12/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "ProcessImageOperation.h"
#import "PiwigoLibrary.h"
#import "PiwigoImage.h"

@interface ProcessImageOperation()

@property (assign,readwrite,atomic) NSInteger piwigoAlbumId;
@property (assign,readwrite,atomic) NSString* imageIdentifier;

@property (strong,readwrite,atomic) LibraryDaemonCommunicator* libraryDaemonCommunicator;
@property (strong,readwrite,atomic) PiwigoCommunicator* piwigoCommunicator;

@property (copy,readwrite,atomic) NSArray* errors;

@end

@implementation ProcessImageOperation

- (id)initWithLibraryDaemonCommunicator:(LibraryDaemonCommunicator*)libraryDaemonCommunicator piwigoCommunicator:(PiwigoCommunicator*)piwigoCommunicator imageIdentifier:(NSString*)imageId piwigoAlbumId:(NSInteger)piwigoAlbumId
{
	self = [super init];
	if (self)
	{
		self.piwigoAlbumId = piwigoAlbumId;
		self.libraryDaemonCommunicator = libraryDaemonCommunicator;
		self.piwigoCommunicator = piwigoCommunicator;
		self.imageIdentifier = imageId;
	}
	return self;
}

- (void)main
{
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	
	__block NSDictionary* objectData = nil;
	[self.libraryDaemonCommunicator getMediaObjectWithIdentifier:self.imageIdentifier completionBlock:^(NSDictionary *gotObjectData) {
		objectData = gotObjectData;
		dispatch_semaphore_signal(sem);
	}];
	dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
	
	if (![[objectData objectForKey:@"mediaType"] isEqualToString:@"image"]) {
		NSLog(@"not an image, bailing out");
		return;
	}
	
	// check if we already have an image
	PiwigoAlbum* album = [[PiwigoLibrary sharedLibrary] getAlbumWithId:self.piwigoAlbumId];
	
	// prepare tag ids
	NSArray* keywords = [objectData valueForKey:@"keywords"];
	NSMutableArray* tagIds = [NSMutableArray array];
	for (NSString* keyword in keywords) {
		// don't create keyword for album name
		if ([keyword isEqualToString:album.name]) {
			continue;
		}
		NSInteger tagId = [[PiwigoLibrary sharedLibrary] getTagId:keyword];
		// create the tag if necessary
		if (tagId != NSNotFound) {
			[tagIds addObject:@(tagId)];
		} else {
			NSLog(@"adding tag: %@", keyword);
			[self.piwigoCommunicator addTagWithName:keyword successBlock:^(NSInteger tagId) {
				[tagIds addObject:@(tagId)];
				dispatch_semaphore_signal(sem);
			} failBlock:^(NSError *err) {
				NSLog(@"unable to create tag %@: %@", keyword, err);
				dispatch_semaphore_signal(sem);
			}];
			dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
		}
	}
	
	__strong typeof(self) sSelf = self;
	__block PiwigoImage* image = [album imageWithIphotoKey:self.imageIdentifier];
	if (image == nil) {
		// need to upload the image
		
		NSURL* url = [NSURL URLWithString:[objectData valueForKey:@"URL"]];
		NSURL* thumbnailUrl = [NSURL URLWithString:[objectData valueForKey:@"thumbnailURL"]];

        NSString* comment = [PiwigoImage commentForIphotoKey:self.imageIdentifier];
		
		NSLog(@"adding image %@ to group %li", self.imageIdentifier, (long)self.piwigoAlbumId);
		[self.piwigoCommunicator addImageAtURL:url thumbnail:thumbnailUrl toAlbum:self.piwigoAlbumId comment:comment tagIds:tagIds successBlock:^(NSInteger newImageId) {
			image = [[PiwigoImage alloc] initWithId:newImageId comment:comment];
			image.tagIds = tagIds;
			
			[album addImage:image];
			dispatch_semaphore_signal(sem);
		} failBlock:^(NSArray *errors) {
			sSelf.errors = errors;
			dispatch_semaphore_signal(sem);
		}];
		
		dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
		
	} else {
        

	
		// update tags if necessary
		if (![[NSSet setWithArray:image.tagIds] isEqualToSet:[NSSet setWithArray:tagIds]]) {
			NSLog(@"updating tags on image %@ to group %li", self.imageIdentifier, (long)self.piwigoAlbumId);
			[self.piwigoCommunicator setTagsOnImageWithId:image.imageId toTagIds:tagIds successBlock:^(NSXMLDocument *imageDocument) {
				image.tagIds = tagIds;
				dispatch_semaphore_signal(sem);
			} failBlock:^(NSError *error) {
				dispatch_semaphore_signal(sem);
				NSLog(@"unable to assign tags: %@", error);
			}];
			dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
		}
		
	}

}

@end
