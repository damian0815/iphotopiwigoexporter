//
//  NSImage+ResizeAndData.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 14/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (ResizeAndData)

- (NSImage *)resizedImageWithSize:(NSSize)newSize;

- (NSBitmapImageRep *)unscaledBitmapImageRep;

- (NSImage *)croppedImageWithSize:(NSSize)outputSize cropRect:(NSRect)sourceRect;

@end
