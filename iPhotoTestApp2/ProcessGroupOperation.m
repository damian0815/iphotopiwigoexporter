//
//  ProcessGroupOperation.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 12/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "ProcessGroupOperation.h"
#import "LibraryDaemonCommunicator.h"
#import "PiwigoCommunicator.h"
#import "PiwigoLibrary.h"
#import "PiwigoAlbum.h"
#import "PiwigoImage.h"

@interface ProcessGroupOperation()

@property (strong,readwrite,atomic) NSString* groupIdentifier;
@property (strong,readwrite,atomic) NSString* groupName;
@property (strong,readwrite,atomic) LibraryDaemonCommunicator* libraryDaemonCommunicator;
@property (strong,readwrite,atomic) PiwigoCommunicator* piwigoCommunicator;

@property (readwrite,atomic,strong) NSDictionary* childGroups;
@property (readwrite,atomic,strong) NSArray* childMediaObjectIds;
@property (readwrite,atomic,assign) NSInteger piwigoAlbumId;

@property (readwrite,atomic,strong) NSError* error;

@end

@implementation ProcessGroupOperation


- (id)initWithLibraryDaemonCommunicator:(LibraryDaemonCommunicator*)daemonCommunicator piwigoCommunicator:(PiwigoCommunicator *)piwigoCommunicator groupName:(NSString *)name groupIdentifier:(NSString *)identifier
{
	self = [super init];
	if (self)
	{
		self.groupIdentifier = identifier;
		self.groupName = name;
		self.piwigoCommunicator = piwigoCommunicator;
		self.libraryDaemonCommunicator = daemonCommunicator;
	}
	return self;
}

- (void)main
{
	[self doIt];
	
	if (self.completionBlock) {
		self.completionBlock();
	}
	self.completionBlock = nil;
}

- (void)doIt
{
	dispatch_semaphore_t sem = dispatch_semaphore_create(0);
	
	PiwigoAlbum* album = [[PiwigoLibrary sharedLibrary] getAlbumWithName:self.groupName];
	if (album) {
		self.piwigoAlbumId = album.albumId;

        if (album.images.count==0) {
            // get album images xml
            [self.piwigoCommunicator getImagesInAlbum:self.piwigoAlbumId successBlock:^(NSXMLDocument *imagesDocument) {
                NSError* err = nil;
                NSArray* imageElements = [imagesDocument nodesForXPath:@"//rsp/images/image" error:&err];
                if (err != nil) {
                    NSLog(@"error %@ parsing images xml for album %@", err, album);
                }
                [album setImagesXml:imageElements];
                dispatch_semaphore_signal(sem);
            } failBlock:^(NSError *error) {
                NSLog(@"unable to get images in album %@: %@", album, error);
                dispatch_semaphore_signal(sem);
            }];
            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        }
        
        for (PiwigoImage* image in album.images) {
            if (image.tagIds.count == 0) {
                // get
                [self.piwigoCommunicator getImageInfoForImageWithId:image.imageId successBlock:^(NSXMLDocument *imageInfo) {
                    NSError* err = nil;
                    // assign tags
                    NSArray* tagNodes = [imageInfo nodesForXPath:@"//rsp/image/tags/tag" error:&err];
                    NSMutableSet* tagIds = [NSMutableSet set];
                    for (NSXMLElement* tagNode in tagNodes) {
                        NSInteger tagId = [[[tagNode attributeForName:@"id"] stringValue] integerValue];
                        [tagIds addObject:@(tagId)];
                    }
                    image.tagIds = [tagIds allObjects];
                    
                    // assign modified date
                    NSArray* modifiedDateNodes = [imageInfo nodesForXPath:@"//rsp/image/tags/tag/lastmodified" error:&err];
                    if (modifiedDateNodes.count) {
                        
                        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
                        NSString* modifiedDateString = [[modifiedDateNodes firstObject] stringValue];
                        image.modifiedDate = [dateFormatter dateFromString:modifiedDateString];
                    }
                    
                    dispatch_semaphore_signal(sem);
                } failBlock:^(NSError *error) {
                    
                    dispatch_semaphore_signal(sem);
                }];
            }
            dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
        }
	} else {
		// create the album
		PiwigoAlbum* rootAlbum = [[PiwigoLibrary sharedLibrary] getAlbumWithName:@"Events"];
		
		__strong typeof(self) sSelf = self;
		NSLog(@"adding album: %@", self.groupName);
		[self.piwigoCommunicator addAlbumWithName:self.groupName parentId:rootAlbum.albumId public:NO successBlock:^(NSXMLDocument *albumDocument) {
			NSError* err = nil;
			NSInteger albumId = [[[[albumDocument nodesForXPath:@"//rsp/id" error:&err] firstObject] stringValue] integerValue];
			if (err != nil) {
				NSLog(@"error %@ getting new album id", err);
				sSelf.error = [NSError errorWithDomain:@"ProcessGroup" code:100 userInfo:nil];
			} else {
				sSelf.piwigoAlbumId = albumId;
				[[PiwigoLibrary sharedLibrary] addAlbum:[[PiwigoAlbum alloc] initWithName:sSelf.groupName id:albumId parentId:rootAlbum.albumId coverImageId:0]];
			}
			dispatch_semaphore_signal(sem);
		} failBlock:^(NSError *error) {
			NSLog(@"unable to creat album for group %@: %@", sSelf.groupName, error);
			sSelf.error = error;
			dispatch_semaphore_signal(sem);
		}];
		
		dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
		
		if (self.error) {
			return;
		}
	}
	
	__block NSDictionary* groupData;
	[self.libraryDaemonCommunicator getGroupWithIdentifier:self.groupIdentifier completionBlock:^(NSDictionary *gotGroupData) {
		groupData = gotGroupData;
		dispatch_semaphore_signal(sem);
	}];
	
	dispatch_semaphore_wait(sem, DISPATCH_TIME_FOREVER);
	
	// check for a piwigo group of the same name
	
	NSDictionary* childGroups = [groupData objectForKey:@"childGroups"];
	if (childGroups.count) {
		self.childGroups = childGroups;
	} else {
		NSArray* mediaObjects = [groupData objectForKey:@"mediaObjects"];
		self.childMediaObjectIds = mediaObjects;
	}
}


@end
