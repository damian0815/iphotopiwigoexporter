//
//  AppDelegate.h
//  iPhotoTestApp2
//
//  Created by Damian Stewart on 26/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

