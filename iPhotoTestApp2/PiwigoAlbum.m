//
//  PiwigoAlbum.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 02/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "PiwigoAlbum.h"
#import "PiwigoImage.h"

@interface PiwigoAlbum()

@property (assign,readwrite,atomic) NSInteger albumId;
@property (assign,readwrite,atomic) NSInteger parentAlbumId;
@property (assign,readwrite,atomic) NSInteger coverImageId;
@property (strong,readwrite,atomic) NSString* name;

@property (strong,readwrite,atomic) NSMutableArray* imagesInternal;

@end

@implementation PiwigoAlbum

- (id)initWithName:(NSString*)name id:(NSInteger)albumId parentId:(NSInteger)parentAlbumId coverImageId:(NSInteger)coverImageId
{
    self = [super init];
    if (self)
    {
        self.name = name;
        self.albumId = albumId;
        self.parentAlbumId = parentAlbumId;
        self.coverImageId = coverImageId;
    }
    return self;
}

- (id)initWithAlbumXml:(NSXMLElement*)element
{
    NSInteger albumId = [[[element attributeForName:@"id"] stringValue] integerValue];
    
    NSError* err = nil;
    NSString* name = ((NSXMLElement*)[[element nodesForXPath:@"name" error:&err] firstObject]).stringValue;
    if (err) {
        NSLog(@"error getting name: %@", err);
    }
    
    err = nil;
    NSArray* parentIDElements = [element nodesForXPath:@"id_uppercat" error:&err];
    if (err) {
        NSLog(@"error getting id_uppercat: %@", err);
    }
    NSInteger parentAlbumId = -1;
    if (parentIDElements.count) {
        parentAlbumId = [[parentIDElements.firstObject stringValue] integerValue];
    }
    
    err = nil;
    NSInteger coverImageId = 0;
    NSArray* coverIdElements = [element nodesForXPath:@"representative_picture_id" error:&err];
    if (err) {
        NSLog(@"error getting representative_picture_id: %@", err);
    } else {
        coverImageId = [[coverIdElements.firstObject stringValue] integerValue];
    }
    
    return [self initWithName:name id:albumId parentId:parentAlbumId coverImageId:coverImageId];
}

- (NSString*)description
{
    return [[super description] stringByAppendingFormat:@":%@:%i:parent=%i:(%i images)",self.name, (int)self.albumId, (int)self.parentAlbumId, (int)self.images.count];
}

- (void)setImagesXml:(NSArray *)imageNodes
{
    @synchronized(self) {
        NSMutableArray* images = [NSMutableArray array];
        for (NSXMLElement* element in imageNodes) {
            PiwigoImage* image = [[PiwigoImage alloc] initWithXml:element];
            [images addObject:image];
        }
        self.imagesInternal = images;
    }
}

- (NSArray*)images
{
	@synchronized(self) {
		return self.imagesInternal;
	}
}

- (void)addImage:(PiwigoImage*)image
{
	@synchronized(self) {
		[self.imagesInternal addObject:image];
	}
}

- (PiwigoImage*)imageWithIphotoKey:(NSString*)key
{
	@synchronized(self) {
		NSInteger existingImageIndex = [self.images indexOfObjectPassingTest:^BOOL(PiwigoImage* obj, NSUInteger idx, BOOL *stop) {
			if ([[obj iPhotoKey] isEqualToString:key]) {
				return YES;
			} else {
				return NO;
			}
		}];
		if (existingImageIndex == NSNotFound) {
			return nil;
		} else {
			return [self.images objectAtIndex:existingImageIndex];
		}
	}
}

- (PiwigoImage*)imageWithId:(NSInteger)imageId
{
	@synchronized(self) {
        NSInteger imageIndex = [self.images indexOfObjectPassingTest:^BOOL(PiwigoImage* obj, NSUInteger idx, BOOL *stop) {
            if (obj.imageId == imageId) {
                return YES;
            } else {
                return NO;
            }
        }];
        if (imageIndex == NSNotFound) {
            return nil;
        } else {
            return [self.images objectAtIndex:imageIndex];
        }
    }
}

@end
