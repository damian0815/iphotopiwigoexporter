//
//  NSImage+ResizeAndData.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 14/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "NSImage+ResizeAndData.h"

@implementation NSImage (ResizeAndData)


- (NSImage*)resizedImageWithSize:(NSSize)newSize
{
	NSImage *sourceImage = self;
	[sourceImage setScalesWhenResized:YES];
	
	// Report an error if the source isn't a valid image
	if (![sourceImage isValid]){
		NSLog(@"Invalid Image");
	} else {
		NSImage *smallImage = [[NSImage alloc] initWithSize: newSize];
		[smallImage lockFocus];
		[sourceImage setSize: newSize];
		[[NSGraphicsContext currentContext] setImageInterpolation:NSImageInterpolationHigh];
		[sourceImage drawAtPoint:NSZeroPoint fromRect:CGRectMake(0, 0, newSize.width, newSize.height) operation:NSCompositeCopy fraction:1.0];
		[smallImage unlockFocus];
		return smallImage;
	}
	return nil;
}

- (NSBitmapImageRep *)unscaledBitmapImageRep
{
    NSImage* image = self;
	
	NSBitmapImageRep *rep = [[NSBitmapImageRep alloc]
							 initWithBitmapDataPlanes:NULL
							 pixelsWide:image.size.width
							 pixelsHigh:image.size.height
							 bitsPerSample:8
							 samplesPerPixel:4
							 hasAlpha:YES
							 isPlanar:NO
							 colorSpaceName:NSDeviceRGBColorSpace
							 bytesPerRow:0
							 bitsPerPixel:0];
	rep.size = image.size;
	
	[NSGraphicsContext saveGraphicsState];
	[NSGraphicsContext setCurrentContext:
	 [NSGraphicsContext graphicsContextWithBitmapImageRep:rep]];
	
	[image drawAtPoint:NSMakePoint(0, 0)
			 fromRect:NSZeroRect
			operation:NSCompositeSourceOver
			 fraction:1.0];
	
	[NSGraphicsContext restoreGraphicsState];
	return rep;
}

- (NSImage*)croppedImageWithSize:(NSSize)outputSize cropRect:(NSRect)sourceRect
{
    NSImage* newImage = [[NSImage alloc] initWithSize:outputSize];
    [newImage lockFocus];
    [self compositeToPoint:NSZeroPoint fromRect:sourceRect operation:NSCompositeCopy];
    [newImage unlockFocus];
    
    return newImage;
}

@end
