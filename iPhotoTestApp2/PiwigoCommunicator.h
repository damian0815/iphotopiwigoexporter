//
//  PiwigoCommunicator.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 28/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PiwigoCommunicator : NSObject

- (id)init UNAVAILABLE_ATTRIBUTE;
- (id)initWithPiwigoUrl:(NSURL*)url username:(NSString*)username password:(NSString*)password;

// wait until AFNetworking queue is empty then call completion block
- (void)waitForCompletionWithBlock:(void (^)())completionBlock;

- (void)loginWithSuccessBlock:(void (^)())successBlock failBlock:(void (^)(NSError* error))failBlock;

- (void)addAlbumWithName:(NSString*)name parentId:(NSInteger)parentId public:(BOOL)isPublic successBlock:(void(^)(NSXMLDocument*))successBlock failBlock:(void(^)(NSError*))failBlock;

//- (void)getAlbumWithName:(NSString*)name successBlock:(void (^)(NSXMLDocument* album))successBlock failBlock:(void (^)(NSError* error))failBlock;

- (void)setAlbumCoverImageForAlbum:(NSInteger)albumId toImage:(NSInteger)imageId successBlock:(void (^)(NSXMLDocument* imagesElement))successBlock failBlock:(void (^)(NSError* error))failBlock;

- (void)getImagesInAlbum:(NSInteger)albumId successBlock:(void (^)(NSXMLDocument* imagesElement))successBlock failBlock:(void (^)(NSError* error))failBlock;

- (void)getImageInfoForImageWithId:(NSInteger)imageId successBlock:(void (^)(NSXMLDocument* imageInfo))successBlock failBlock:(void (^)(NSError* error))failBlock;

- (void)addImageAtURL:(NSURL *)fileUrl thumbnail:(NSURL*)thumbnailUrl toAlbum:(NSInteger)albumId comment:(NSString *)comment tagIds:(NSArray*)tagIds successBlock:(void (^)(NSInteger newImageId))successBlock failBlock:(void (^)(NSArray * errors))failBlock;

- (void)addImageWithData:(NSData*)data toAlbum:(NSInteger)albumId comment:(NSString *)comment tagIds:(NSArray*)tagIds successBlock:(void (^)(NSInteger newImageId))successBlock failBlock:(void (^)(NSArray * errors))failBlock;

- (void)addTagWithName:(NSString*)name successBlock:(void(^)(NSInteger tagId))successBlock failBlock:(void (^)(NSError* err))failBlock;

- (void)setTagsOnImageWithId:(NSInteger)imageId toTagIds:(NSArray*)tagIds successBlock:(void (^)(NSXMLDocument*))successBlock failBlock:(void (^)(NSError * error))failBlock;

- (void)getAlbumsWithSuccessBlock:(void (^)(NSXMLDocument*))successBlock failBlock:(void (^)(NSError *))failBlock;
- (void)getTagsWithSuccessBlock:(void (^)(NSXMLDocument*))successBlock failBlock:(void (^)(NSError *))failBlock;

@end
