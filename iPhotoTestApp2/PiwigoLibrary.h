//
//  PiwigoLibrary.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 02/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PiwigoAlbum.h"

@interface PiwigoLibrary : NSObject

+ (BOOL)sharedLibraryInitialized;
+ (PiwigoLibrary*)sharedLibrary;

- (id)init UNAVAILABLE_ATTRIBUTE;
- (id)initWithAlbumsXml:(NSXMLDocument*)albums tagsXml:(NSXMLDocument*)tags;

- (NSArray*)allTagNames;

- (PiwigoAlbum*)getAlbumWithId:(NSInteger)albumId;
- (PiwigoAlbum*)getAlbumWithName:(NSString*)name;
- (NSArray*)getChildAlbumsOfAlbum:(NSInteger)albumId;

- (void)addAlbum:(PiwigoAlbum*)album;

// returns NSNotFound if not a known tag
- (NSInteger)getTagId:(NSString*)tag;

- (void)addTag:(NSString*)tag withId:(NSInteger)tagId;

@end
