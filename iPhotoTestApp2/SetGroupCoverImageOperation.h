//
//  SetGroupCoverImageOperation.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 14/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PiwigoCommunicator.h"
#import "LibraryDaemonCommunicator.h"

@interface SetGroupCoverImageOperation : NSOperation

- (id)initWithLibraryDaemonCommunicator:(LibraryDaemonCommunicator*)libraryDaemonCommunicator piwigoCommunicator:(PiwigoCommunicator*)communicator albumId:(NSInteger)piwigoAlbumId groupIdentifier:(NSString*)groupIdentifier;

@end
