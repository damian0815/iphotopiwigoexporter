//
//  PiwigoLibrary.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 02/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "PiwigoLibrary.h"
#import "PiwigoAlbum.h"

@interface PiwigoLibrary()

@property (strong,readwrite,atomic) PiwigoAlbum* rootAlbum;
@property (strong,readwrite,atomic) NSMutableArray* albums;

// dictionary of string -> int id
@property (strong,readwrite,atomic) NSMutableDictionary* tags;

@end


@implementation PiwigoLibrary

static PiwigoLibrary* instance = nil;

+ (BOOL)sharedLibraryInitialized
{
    return instance != nil;
}

+ (PiwigoLibrary*)sharedLibrary
{
	NSAssert(instance, @"no instance yet");
	return instance;
}

- (id)initWithAlbumsXml:(NSXMLDocument*)albums tagsXml:(NSXMLDocument*)tags
{
	NSAssert(!instance, @"Already created an instance");
    self = [super init];
    if (self)
    {
        self.albums = [NSMutableArray array];
        NSError* err = nil;
        NSArray* albumNodes = [albums nodesForXPath:@"//rsp/categories/category" error:&err];
        if (err) {
            NSLog(@"error %@ getting categories", err);
        } else {
            for (NSXMLElement* albumElement in albumNodes) {
                PiwigoAlbum* album = [[PiwigoAlbum alloc] initWithAlbumXml:albumElement];
                NSLog(@"found album %@", album);
                [self addAlbum:album];
            }
        }
        
        self.tags = [NSMutableDictionary dictionary];
        NSArray* tagNodes = [tags nodesForXPath:@"//rsp/tags/tag" error:&err];
        if (err) {
            NSLog(@"error %@ getting tags", err);
        } else {
            for (NSXMLElement* tagElement in tagNodes) {
                NSInteger tagId = [[[tagElement attributeForName:@"id"] stringValue] integerValue];
                NSString* tagName = [[tagElement attributeForName:@"name"] stringValue];
                NSLog(@"found tag %i:%@", (int)tagId, tagName);
                [self.tags setObject:@(tagId) forKey:tagName];
            }
        }
    }
	instance = self;
    return self;
}

- (NSArray*)allTagNames
{
    return [self.tags allKeys];
}

- (PiwigoAlbum*)getAlbumWithName:(NSString*)name
{
    for (PiwigoAlbum* album in self.albums) {
        if ([album.name isEqualToString:name]) {
            return album;
        }
    }
    return nil;
}

- (PiwigoAlbum*)getAlbumWithId:(NSInteger)albumId
{
    for (PiwigoAlbum* album in self.albums) {
		if (album.albumId == albumId) {
			return album;
		}
	}
	return nil;
}

- (NSArray*)getChildAlbumsOfAlbum:(NSInteger)albumId
{
    NSMutableArray* children = [NSMutableArray array];
    for (PiwigoAlbum* album in self.albums) {
        if (album.parentAlbumId == albumId) {
            [children addObject:album];
        }
    }
    return children;
}

- (void)addAlbum:(PiwigoAlbum*)album
{
    [self.albums addObject:album];
    if (self.rootAlbum == nil || self.rootAlbum.parentAlbumId == album.albumId) {
        self.rootAlbum = album;
    }
}

- (void)addTag:(NSString*)tag withId:(NSInteger)tagId
{
    [self.tags setObject:@(tagId) forKey:tag];
}

- (NSInteger)getTagId:(NSString*)tag
{
    NSNumber* tagId = [self.tags objectForKey:tag];
    if (tagId) {
        return [tagId integerValue];
    } else {
        return NSNotFound;
    }
}

@end
