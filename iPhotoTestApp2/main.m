//
//  main.m
//  iPhotoTestApp2
//
//  Created by Damian Stewart on 26/02/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
