//
//  LibraryDaemonCommunicator.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 10/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "LibraryDaemonCommunicator.h"
#import <sys/types.h>
#import <sys/socket.h>
#import <arpa/inet.h>

@interface LibraryDaemonCommunicator()

@property (assign,readwrite,atomic) int port;

@end

@implementation LibraryDaemonCommunicator


- (id)initWithPort:(int)port
{
	self = [super init];
	if ( self ) {
		self.port = port;
	}
	return self;
}

- (void)getRootGroupWithCompletionBlock:(void (^)(NSDictionary* rootGroupData)) completionBlock
{
	[self makeRequestWithMethod:@"getRootGroup" params:@{} completionBlock:completionBlock];
}

- (void)getGroupWithIdentifier:(NSString*)identifier completionBlock:(void (^)(NSDictionary* groupData))completionBlock
{
    [self makeRequestWithMethod:@"getGroup" params:@{@"identifier":identifier} completionBlock:completionBlock];
}

- (void)getMediaObjectWithIdentifier:(NSString*)identifier completionBlock:(void (^)(NSDictionary* objectData))completionBlock
{
    [self makeRequestWithMethod:@"getMediaObject" params:@{@"objectIdentifier":identifier} completionBlock:completionBlock];
}

- (void)makeRequestWithMethod:(NSString*)method params:(NSDictionary*)params completionBlock:(void (^)(id object))completionBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(){
        NSDictionary* requestDictionary = @{@"method": method, @"params": params};
        NSError* err = nil;
        NSData* data = [NSJSONSerialization dataWithJSONObject:requestDictionary options:0 error:&err];
        if (err) {
            NSLog(@"error serializing: %@", err);
            completionBlock(nil);
        }
        int socketFd = socket(PF_INET, SOCK_STREAM, 0);
        
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(addr));
        addr.sin_family = PF_INET;
        addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
        addr.sin_port = htons(self.port);
        
        int result = connect(socketFd, (struct sockaddr*)&addr, sizeof(addr));
        if (result!=0) {
            perror("Socket creation");
            completionBlock(nil);
			return;
        }
        
        // connected
		ssize_t written = 0;
		while (written < [data length]) {
			ssize_t wrote = send(socketFd, [data bytes]+written, [data length]-written, 0);
			if (wrote>=0) {
				written += wrote;
			} else {
				perror("writing to socket");
				NSLog(@"error writing: %i", errno);
				shutdown(socketFd, SHUT_WR);
				close(socketFd);
				completionBlock(nil);
			}
        }
        
        //NSLog(@"wrote %i bytes", (int)written);
        
        shutdown(socketFd, SHUT_WR);
        
        // read result
        NSMutableData* receivedData = [NSMutableData data];
        errno = 0;
        while (true) {
            char byte;
            ssize_t bytesRead = read(socketFd, &byte, 1);
            if (bytesRead==-1 || bytesRead==0) {
				if (errno != 0) {
					perror("while reading");
				}
                break;
            } else if (bytesRead>0) {
                //NSLog(@"read %i: %c", byte, byte);
                [receivedData appendBytes:&byte length:bytesRead];
            }
        }
        
        //NSLog(@"read %lu bytes", (unsigned long)receivedData.length);
        
        
        // deserialize json
        err = nil;
        id receivedObject = [NSJSONSerialization JSONObjectWithData:receivedData options:0 error:&err];
        if (err) {
            NSLog(@"error deserializing json: %@", err);
        }
        
        completionBlock(receivedObject);
        
    });
                   
	
}

@end
