//
//  MainWindowController.m
//  iPhotoExportTest
//
//  Created by Damian Stewart on 07/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "MainWindowController.h"
#import "IphotoToPiwigoPusher.h"

@interface MainWindowController()<NSTextFieldDelegate>

@property (weak) IBOutlet NSTextField *piwigoGalleryURLTextField;
@property (weak) IBOutlet NSTextField *usernameTextField;
@property (weak) IBOutlet NSSecureTextField *passwordTextField;
@property (weak) IBOutlet NSProgressIndicator *progressBar;
@property (weak) IBOutlet NSButton *startButton;

@property (strong,readwrite,atomic) IphotoToPiwigoPusher* iPhotoToPiwigoPusher;

@end

@implementation MainWindowController

- (id)initWithWindow:(NSWindow *)window
{
    self = [super initWithWindow:window];
    if (self) {
        // Initialization code here.
        
        
    }
    return self;
}

- (void)windowDidLoad
{
    [super windowDidLoad];
    
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    NSString* savedPiwigoUrl = [[NSUserDefaults standardUserDefaults] valueForKey:@"piwigoUrl"];
	if (savedPiwigoUrl) {
		self.piwigoGalleryURLTextField.stringValue = savedPiwigoUrl;
	}
	NSString* savedPiwigoUsername = [[NSUserDefaults standardUserDefaults] valueForKey:@"piwigoUsername"];
	if (savedPiwigoUsername) {
		self.usernameTextField.stringValue = savedPiwigoUsername;
	}
	NSString* savedPiwigoPassword = [[NSUserDefaults standardUserDefaults] valueForKey:@"piwigoPassword"];
	if (savedPiwigoPassword) {
		self.passwordTextField.stringValue = savedPiwigoPassword;
	}

}
- (void)onUrlFieldUpdated
{
    NSString* url = self.piwigoGalleryURLTextField.stringValue;
    [[NSUserDefaults standardUserDefaults] setObject:url forKey:@"piwigoUrl"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)onPasswordFieldUpdated
{
    NSString* password = self.passwordTextField.stringValue;
    [[NSUserDefaults standardUserDefaults] setObject:password forKey:@"piwigoPassword"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
- (void)onUsernameFieldUpdated
{
    NSString* username = self.usernameTextField.stringValue;
    [[NSUserDefaults standardUserDefaults] setObject:username forKey:@"piwigoUsername"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (IBAction)startButtonPressed:(id)sender
{
	if (self.iPhotoToPiwigoPusher == nil) {
		[self setUIEnabled:NO];
		
        [self.progressBar setIndeterminate:YES];
        
		self.iPhotoToPiwigoPusher = [[IphotoToPiwigoPusher alloc] initWithPiwigoUrl:self.piwigoGalleryURLTextField.stringValue username:self.usernameTextField.stringValue password:self.passwordTextField.stringValue];
		__strong typeof(self) sSelf = self;
		self.iPhotoToPiwigoPusher.completionBlock = ^(BOOL cancelled) {
			[sSelf setUIEnabled:YES];
			sSelf.iPhotoToPiwigoPusher = nil;
            sSelf.progressBar.doubleValue = 0;
			sSelf.startButton.title = @"Start";
		};
		
		NSProgressIndicator* progressBar = self.progressBar;
		self.iPhotoToPiwigoPusher.progressBlock = ^(CGFloat progress) {
			//NSLog(@"progress: %6.2f%%", progress*100.0f);
            [sSelf.progressBar setIndeterminate:NO];
			[progressBar setDoubleValue:progress];
		};
		
		self.startButton.title = @"Cancel";
		[self.iPhotoToPiwigoPusher start];
		
	} else {
		[self.iPhotoToPiwigoPusher cancel];
	}
}

- (void)setUIEnabled:(BOOL)enabled
{
	[self.usernameTextField setEnabled:enabled];
	[self.piwigoGalleryURLTextField setEnabled:enabled];
	[self.passwordTextField setEnabled:enabled];
    [self.progressBar setHidden:enabled];
}


- (void)controlTextDidChange:(NSNotification *)note
{
	if (note.object == self.usernameTextField) {
		[self onUsernameFieldUpdated];
	} else if (note.object == self.passwordTextField) {
		[self onPasswordFieldUpdated];
	} else if (note.object == self.piwigoGalleryURLTextField) {
		[self onUrlFieldUpdated];
	}
}


@end
