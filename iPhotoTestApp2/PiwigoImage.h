//
//  PiwigoImage.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 03/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PiwigoImage : NSObject

/// iPhoto key corresponding to this image as read from comments field. may be null.
@property (readonly,atomic) NSString* iPhotoKey;

@property (readonly,atomic) NSInteger imageId;
@property (readonly,atomic) NSString* comment;
@property (copy,readwrite,atomic) NSArray* tagIds;
@property (copy,readwrite,atomic) NSDate* modifiedDate;
@property (readonly,atomic) NSSize size;

+ (NSString*)commentForIphotoKey:(NSString*)iPhotoKey;

- (id)initWithId:(NSInteger)imageId comment:(NSString*)comment;
- (id)initWithXml:(NSXMLElement*)imageXml;

@end
