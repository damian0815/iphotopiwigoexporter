//
//  ProcessGroupOperation.h
//  iPhotoExportTest
//
//  Created by Damian Stewart on 12/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Foundation/Foundation.h>

@class LibraryDaemonCommunicator;
@class PiwigoCommunicator;

@interface ProcessGroupOperation : NSOperation

@property (readonly,atomic,strong) NSDictionary* childGroups;
@property (readonly,atomic,strong) NSArray* childMediaObjectIds;
@property (readonly,atomic,assign) NSInteger piwigoAlbumId;

@property (readonly,atomic,strong) NSError* error;

- (id)initWithLibraryDaemonCommunicator:(LibraryDaemonCommunicator*)daemonCommunicator piwigoCommunicator:(PiwigoCommunicator*)piwigoCommunicator groupName:(NSString*)name groupIdentifier:(NSString*)identifier;

@end
