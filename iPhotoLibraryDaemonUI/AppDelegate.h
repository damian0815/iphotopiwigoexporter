//
//  AppDelegate.h
//  iPhotoLibraryDaemonUI
//
//  Created by Damian Stewart on 08/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

