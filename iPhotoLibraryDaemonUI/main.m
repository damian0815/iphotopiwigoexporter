//
//  main.m
//  iPhotoLibraryDaemonUI
//
//  Created by Damian Stewart on 08/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
	return NSApplicationMain(argc, argv);
}
