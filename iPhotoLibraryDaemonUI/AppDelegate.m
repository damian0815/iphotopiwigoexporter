//
//  AppDelegate.m
//  iPhotoLibraryDaemonUI
//
//  Created by Damian Stewart on 08/03/15.
//  Copyright (c) 2015 ds. All rights reserved.
//

#import "AppDelegate.h"
#import "IphotoLibraryDaemon.h"

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;

@property (strong,readwrite,atomic) IphotoLibraryDaemon* daemon;

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
	// Insert code here to initialize your application
	self.daemon = [[IphotoLibraryDaemon alloc] init];
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
	// Insert code here to tear down your application
}

@end
